
# DO NOT EDIT THIS FILE!
# This script was auto-generated by running the command:
# ../../pycf/generateLibCFConfig.py -b .. -s ../.. -c libCFConfig.py on Fri Mar 23 14:08:25 2012

try: CPPFLAGS = "-I/home/pletzer/software/netcdf-4.1.1-ser/include"
except: CPPFLAGS = None
try: prefix = "/home/pletzer/software/libcf-debug"
except: prefix = None
USE_NETCDF4 = 0
USE_NETCDF4 = 0
# netcdf constants
NC_NAT = 0
NC_BYTE = 1
NC_CHAR = 2
NC_SHORT = 3
NC_INT = 4
NC_FLOAT = 5
NC_DOUBLE = 6
NC_FILL_BYTE = -127
NC_FILL_CHAR = 0
NC_FILL_SHORT = -32767
NC_FILL_INT = -2147483647
NC_FILL_FLOAT = 9.9692099683868690e+36
NC_FILL_DOUBLE = 9.9692099683868690e+36
NC_FILL = 0
NC_NOFILL = 0x100
NC_NOWRITE = 0
NC_WRITE = 0x1
NC_CLOBBER = 0
NC_NOCLOBBER = 0x4
NC_64BIT_OFFSET = 0x0200
NC_SHARE = 0x0800
NC_STRICT_NC3 = 0x8
NC_LOCK = 0x0400
NC_FORMAT_CLASSIC = 1
NC_FORMAT_64BIT = 2
NC_FORMAT_NETCDF4 = 3
NC_FORMAT_NETCDF4_CLASSIC = 4
NC_SIZEHINT_DEFAULT = 0
NC_UNLIMITED = 0
NC_GLOBAL = -1
NC_MAX_DIMS = 1024
NC_MAX_ATTRS = 8192
NC_MAX_VARS = 8192
NC_MAX_NAME = 256
try: NC_MAX_VAR_DIMS = NC_MAX_DIMS
except: NC_MAX_VAR_DIMS = None
NC_NOERR = 0
NC2_ERR = -1
NC_EBADID = -33
NC_ENFILE = -34
NC_EEXIST = -35
NC_EINVAL = -36
NC_EPERM = -37
NC_ENOTINDEFINE = -38
NC_EINDEFINE = -39
NC_EINVALCOORDS = -40
NC_EMAXDIMS = -41
NC_ENAMEINUSE = -42
NC_ENOTATT = -43
NC_EMAXATTS = -44
NC_EBADTYPE = -45
NC_EBADDIM = -46
NC_EUNLIMPOS = -47
NC_EMAXVARS = -48
NC_ENOTVAR = -49
NC_EGLOBAL = -50
NC_ENOTNC = -51
NC_ESTS = -52
NC_EMAXNAME = -53
NC_EUNLIMIT = -54
NC_ENORECVARS = -55
NC_ECHAR = -56
NC_EEDGE = -57
NC_ESTRIDE = -58
NC_EBADNAME = -59
NC_ERANGE = -60
NC_ENOMEM = -61
NC_EVARSIZE = -62
NC_EDIMSIZE = -63
NC_ETRUNC = -64
NC_EAXISTYPE = -65
NC_EDAP = -66
NC_ECURL = -67
NC_EIO = -68
NC_ENODATA = -69
NC_EDAPSVC = -70
NC_EDAS = -71
NC_EDDS = -72
NC_EDATADDS = -73
NC_EDAPURL = -74
NC_EDAPCONSTRAINT = -75
NC_TURN_OFF_LOGGING = -1
try: FILL_BYTE = NC_FILL_BYTE
except: FILL_BYTE = None
try: FILL_CHAR = NC_FILL_CHAR
except: FILL_CHAR = None
try: FILL_SHORT = NC_FILL_SHORT
except: FILL_SHORT = None
try: FILL_LONG = NC_FILL_INT
except: FILL_LONG = None
try: FILL_FLOAT = NC_FILL_FLOAT
except: FILL_FLOAT = None
try: FILL_DOUBLE = NC_FILL_DOUBLE
except: FILL_DOUBLE = None
try: MAX_NC_DIMS = NC_MAX_DIMS
except: MAX_NC_DIMS = None
try: MAX_NC_ATTRS = NC_MAX_ATTRS
except: MAX_NC_ATTRS = None
try: MAX_NC_VARS = NC_MAX_VARS
except: MAX_NC_VARS = None
try: MAX_NC_NAME = NC_MAX_NAME
except: MAX_NC_NAME = None
try: MAX_VAR_DIMS = NC_MAX_VAR_DIMS
except: MAX_VAR_DIMS = None
try: NC_LONG = NC_INT
except: NC_LONG = None
try: NC_ENTOOL = NC_EMAXNAME
except: NC_ENTOOL = None
NC_EXDR = -32
NC_SYSERR = -31
NC_FATAL = 1
NC_VERBOSE = 2
# libCF constants
COORDINATE_AXIS_TYPE = "_CoordinateAxisType"
COORDINATE_AXES = "_CoordinateAxes"
COORDINATE_Z_IS_POSITIVE = "_CoordinateZisPositive"
Z_UP = "up"
Z_DOWN = "down"
COORDINATE_SYSTEMS = "_CoordinateSystems"
TRANSFORM_NAME = "transform_name"
TRANSFORM_TYPE = "_CoordinateTransformType"
COORDINATE_TRANSFORMS = "_CoordinateTransforms"
CF_LONGITUDE_NAME = "longitude"
CF_LATITUDE_NAME = "latitude"
CF_LONGITUDE_UNITS = "degrees_east"
CF_LATITUDE_UNITS = "degrees_north"
CF_LONGITUDE_AXIS = "X"
CF_LATITUDE_AXIS = "Y"
CF_LEVEL_AXIS = "Z"
CF_TIME_AXIS = "T"
CF_UNITS = "units"
CF_FORMULA_TERMS = "formula_terms"
CF_AXIS = "axis"
CF_UP = "up"
CF_DOWN = "down"
CF_POSITIVE = "positive"
CF_MAX_LEN = 40
CF_MAX_GS_STRING = 255
CF_MAX_FT_VARS = 7
try: CF_MAX_FT_LEN = NC_MAX_NAME * CF_MAX_FT_VARS + 31
except: CF_MAX_FT_LEN = None
CF_MAX_COORDS = 10
try: CF_MAX_COORD_LEN = NC_MAX_NAME * CF_MAX_COORDS
except: CF_MAX_COORD_LEN = None
NCCF_NOAXISTYPE = 0
NCCF_LATITUDE = 1
NCCF_LONGITUDE = 2
NCCF_GEOX = 3
NCCF_GEOY = 4
NCCF_GEOZ = 5
NCCF_HEIGHT_UP = 6
NCCF_HEIGHT_DOWN = 7
NCCF_PRESSURE = 8
NCCF_TIME = 9
NCCF_RADAZ = 10
NCCF_RADEL = 11
NCCF_RADDIST = 12
try: CF_NOERR = NC_NOERR
except: CF_NOERR = None
try: CF_ENOMEM = NC_ENOMEM
except: CF_ENOMEM = None
try: CF_EBADTYPE = NC_EBADTYPE
except: CF_EBADTYPE = None
try: CF_EINVAL = NC_EINVAL
except: CF_EINVAL = None
CF_ENETCDF = -400
CF_ETIME = -401
CF_EMINMAX = -402
CF_EUNKCOORD = -403
CF_EEXISTS = -404
CF_ENOTFOUND = -405
CF_ENODIM = -406
CF_EBADVALUE = -407
CF_EMAXFT = -408
CF_EBADFT = -409
CF_EMAX_NAME = -410
CF_EMAXCOORDS = -411
CF_ERECTOOLARGE = -412
CF_ENDIMS = -413
CF_EBADRANGE = -414
X_SCHAR_MAX = 127
X_UCHAR_MAX = 255
X_SHORT_MAX = 32767
X_INT_MAX = 2147483647
X_FLOAT_MAX = 3.402823466e+38
try: X_FLOAT_MIN = -X_FLOAT_MAX
except: X_FLOAT_MIN = None
X_DOUBLE_MAX = 1.7976931348623157e+308
try: X_DOUBLE_MIN = -X_DOUBLE_MAX
except: X_DOUBLE_MIN = None
CF_BYTE_MIN = -126
try: CF_BYTE_MAX = X_SCHAR_MAX
except: CF_BYTE_MAX = None
CF_CHAR_MIN = 1
try: CF_CHAR_MAX = X_UCHAR_MAX
except: CF_CHAR_MAX = None
CF_SHORT_MIN = -32766
try: CF_SHORT_MAX = X_SHORT_MAX
except: CF_SHORT_MAX = None
CF_INT_MIN = -2147483646
try: CF_INT_MAX = X_INT_MAX
except: CF_INT_MAX = None
try: CF_FLOAT_MIN = X_FLOAT_MIN
except: CF_FLOAT_MIN = None
try: CF_FLOAT_MAX = NC_FILL_FLOAT
except: CF_FLOAT_MAX = None
try: CF_DOUBLE_MIN = NC_FILL_DOUBLE
except: CF_DOUBLE_MIN = None
try: CF_DOUBLE_MAX = X_DOUBLE_MIN
except: CF_DOUBLE_MAX = None
CF_CONVENTIONS = "Conventions"
CF_CONVENTION_STRING = "CF-1.0"
CF_TITLE = "title"
CF_HISTORY = "history"
CF_INSTITUTION = "institution"
CF_SOURCE = "source"
CF_COMMENT = "comment"
CF_REFERENCES = "references"
CF_UNITS = "units"
CF_LONG_NAME = "long_name"
CF_STANDARD_NAME = "standard_name"
CF_FILL_VALUE = "_FillValue"
CF_VALID_MIN = "valid_min"
CF_VALID_MAX = "valid_max"
CF_COORDINATES = "coordinates"
CF_VERT_ATM_LN = 0
STD_ATM_LN = "atmosphere_ln_pressure_coordinate"
FT_ATM_LN_FORMAT = "p0: %s lev: %s"
FT_ATM_LN_TERMS = 2
CF_VERT_SIGMA = 1
STD_SIGMA = "atmosphere_sigma_coordinate"
FT_SIGMA_FORMAT = "sigma: %s ps: %s ptop: %s"
FT_SIGMA_TERMS = 3
CF_VERT_HYBRID_SIGMA = 2
STD_HYBRID_SIGMA = "atmosphere_hybrid_sigma_pressure_coordinate"
FT_HYBRID_SIGMA_FORMAT = "a: %s b: %s ps: %s p0: %s"
FT_HYBRID_SIGMA_TERMS = 4
CF_VERT_HYBRID_HEIGHT = 3
STD_HYBRID_HEIGHT = "atmosphere_hybrid_height_coordinate"
FT_HYBRID_HEIGHT_FORMAT = "a: %s b: %s orog: %s"
FT_HYBRID_HEIGHT_TERMS = 3
CF_VERT_SLEVE = 4
STD_SLEVE = "atmosphere_sleve_coordinate"
FT_SLEVE_FORMAT = "a: %s b1: %s b2: %s ztop: %s zsurf1: %s zsurf2: %s"
FT_SLEVE_TERMS = 6
CF_VERT_OCEAN_SIGMA = 5
STD_OCEAN_SIGMA = "ocean_sigma_coordinate"
FT_OCEAN_SIGMA_FORMAT = "sigma: %s eta: %s depth: %s"
FT_OCEAN_SIGMA_TERMS = 3
CF_VERT_OCEAN_S = 6
STD_OCEAN_S = "ocean_s_coordinate"
FT_OCEAN_S_FORMAT = "s: %s eta: %s depth: %s a: %s b: %s depth_c: %s"
FT_OCEAN_S_TERMS = 6
CF_VERT_OCEAN_SIGMA_Z = 7
STD_OCEAN_SIGMA_Z = "ocean_sigma_z_coordinate"
FT_OCEAN_SIGMA_Z_FORMAT = "sigma: %s eta: %s depth: %s depth_c: %s nsigma: %s zlev: %s"
FT_OCEAN_SIGMA_Z_TERMS = 6
CF_VERT_OCEAN_DBL_SIGMA = 8
STD_OCEAN_DBL_SIGMA = "ocean_double_sigma_coordinate"
FT_OCEAN_DBL_SIGMA_FORMAT = "sigma: %s depth: %s z1: %s z2: %s a: %s href: %s k_c: %s"
FT_OCEAN_DBL_SIGMA_TERMS = 7
CF_NUM_VERT = 9
FT_MAX_TERMS = 7
CF_HUGE_INT = 2147483647
CF_HUGE_FLOAT = 3.402823466e+38
try: CF_HUGE_DOUBLE = HUGE_VAL
except: CF_HUGE_DOUBLE = None
CF_GS_HOST_MOSAIC_FILENAME = "gridspec_mosaic_filename"
CF_GS_HOST_TILE_FILENAMES = "gridspec_tile_filenames"
CF_GS_HOST_STATDATA_FILENAME = "gridspec_static_data_filenames"
CF_GS_HOST_TIMEDATA_FILENAME = "gridspec_time_data_filenames"
CF_GLATT_FILETYPE_HOST = "gridspec_host_file"
CF_GS_MOSAIC_CONTACT_MAP = "gridspec_contact_map"
CF_GS_MOSAIC_COORDINATE_NAME = "gridspec_coord_names"
CF_GS_MOSAIC_TILE_NAMES = "gridspec_tile_names"
CF_GS_MOSAIC_TILE_CONTACTS = "gridspec_tile_contacts"
CF_GLATT_FILETYPE_MOSAIC = "gridspec_mosaic_file"
CF_CONTACT_FORMAT = "gridspec_slice_format"
CF_INDEX_SEPARATOR = " "
CF_TILE_SEPARATOR = " | "
CF_RANGE_SEPARATOR = ":"
CF_GLATT_FILETYPE_GRID = "gridspec_tile_file"
CF_GLATT_FILETYPE_STATIC_DATA = "gridspec_static_data_file"
CF_GLATT_FILETYPE_TIME_DATA = "gridspec_time_data_file"
CF_DIMNAME_STRING = "nstring"
CF_DIMNAME_MOSAIC = "ncontact"
CF_DIMNAME_NDIMS = "ndims"
CF_DIMNAME_NTIMEDATA = "ntimedata"
CF_DIMNAME_NSTATDATA = "nstatdata"
CF_DIMNAME_NGRIDS = "ngrid"
CF_DIMNAME_NCONTACTS = "ncontacts"
CF_DIMNAME_NTARGETS = "ntargets"
CF_DIMNAME_NNODES = "nnodes"
CF_DIMNAME_NTIMES = "ntimes"
CF_ATTNAME_CF_TYPE_NAME = "gridspec_type_name"
CF_ATTNAME_STANDARD_NAME = "standard_name"
CF_ATTNAME_LONG_NAME = "long_name"
CF_ATTNAME_UNITS = "units"
CF_ATTNAME_BOUNDS = "bounds"
CF_ATTNAME_AXIS = "axis"
CF_ATTNAME_POSITIVE = "positive"
CF_GLOBAL = ""
CF_NA = "N/A"
CF_COORDINATES_ID = "gridspec_coordinates_id"
CF_DATA_ID = "gridspec_data_id"
CF_FILETYPE = "gridspec_file_type"
CF_GRIDNAME = "gridspec_tile_name"
CF_ORIGINALFILENAME = "originalfilename"
CF_GLOBAL_TITLE = "Title"
CF_GLOBAL_IDENTIFIER = "Identifier"
CF_GLOBAL_INSTITUTION = "Institution"
CF_GLOBAL_MODEL = "Model"
CF_GLOBAL_RUN = "Run"
CF_FILENAME_GRID = "_grid"
CF_FILENAME_CONTACTS = "_contacts"
CF_FILENAME_MOSAIC = "_mosaic"
try: STRING_SIZE = NC_MAX_NAME
except: STRING_SIZE = None
NCCF_ENOAXISID = -1000
NCCF_ENOCOORDID = -1001
NCCF_ENOGRIDID = -1002
NCCF_ENODATAID = -1003
NCCF_ENOREGRIDID = -1004
NCCF_ENOMOSAICID = -1005
NCCF_ENOHOSTID = -1006
NCCF_VAROBJCREATE = -1007
NCCF_VAROBJCREATEFROMFILE = -1008
NCCF_VAROBJDESTROY = -1009
NCCF_VAROBJSETATTRIB = -1010
NCCF_VAROBJGETATTRIBPTR = -1011
NCCF_VAROBJSETDIMS = -1012
NCCF_VAROBJGETDIMNAMEPTR = -1013
NCCF_VAROBJSETDATAPTR = -1014
NCCF_VAROBJSETDATA = -1015
NCCF_VAROBJGETVARNAMEPTR = -1016
NCCF_VAROBJGETDATATYPE = -1017
NCCF_VAROBJGETDATAPTR = -1018
NCCF_VAROBJGETNUMDIMS = -1019
NCCF_VAROBJGETATTRIBLIST = -1020
NCCF_VAROBJSETVARNAME = -1021
NCCF_VAROBJWRITELISTOFVARS = -1022
NCCF_EINDEXOUTOFRANGE = -1030
NCCF_EPARSERANGES = -1031
NCCF_ENOTHOSTFILE = -1040
NCCF_EPUTCOORD = -1041
NCCF_EPUTGRID = -1042
NCCF_EPUTDATA = -1043
NCCF_EPUTREGRID = -1044
NCCF_EPUTMOSAIC = -1045
NCCF_EPUTHOST = -1046
NCCF_ENODATA = -1050
NCCF_EVERTMISMATCH = -1051
NCCF_ENGRIDMISMATCH = -1052
NCCF_EATTEXISTS = -1053
NCCF_EBADGRIDINDEX = -1054
NCCF_EBADVAR = -1055
NCCF_ENDIMS = -1056
NCCF_LISTITEMEXISTS = -1060 
